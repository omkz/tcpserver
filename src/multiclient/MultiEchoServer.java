/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package multiclient;

/**
 *
 * @author omenkzz
 */
import java.io.*;
import java.net.*;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

public class MultiEchoServer {

    private static ServerSocket servSock;
    private static final int PORT = 16061;

    public static void main(String args[]) throws IOException {
        System.out.println("Opening Port.....\n");
        try {
            servSock = new ServerSocket(PORT);
        } catch (IOException e) {
            System.out.println("Unable to attach to port");
            System.exit(1);
        }
        do {
            Socket client = servSock.accept();
            ClientHandler handler = new ClientHandler(client);
            handler.start();
        } while (true);
    }
}

class ClientHandler extends Thread {

    private Socket client;
    private BufferedReader in;
    private PrintWriter out;
    private static Logger log = Logger.getLogger("Logging Untuk Server");

    public ClientHandler(Socket socket) throws IOException {
        client = socket;
        try {
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            out = new PrintWriter(client.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Handler handler = new FileHandler("server.log");
        log.addHandler(handler);

    }

    public void run() {
        try {
            String received;
            do {
                received = in.readLine();
                log.info(received);
                System.out.println(received);
                out.println("SERVER : " + received);
            } while (!received.equals("QUIT"));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (client != null) {
                    System.out.println("Closing down connection");
                    client.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
